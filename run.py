#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask
from flask import jsonify
from flask import request
from io import open
import json
import sys




app = Flask(__name__, static_folder='www')

@app.route('/<path:path>')
def static_file(path):
    try:
        return app.send_static_file(path)
    except:
        return "Error"

#@app.route('/favorit', methods=['POST'])
#def set_favorit():
    # search = request.args.get("search")
    # page = request.args.get("page")
    # email = request.form.get('email')
    # password = request.form.get('password')
    #   data = request.data
    #  movie = json.loads(data)
    # print(movie['_id'], movie['title'])
    #rez = {'rez': "ok"}
    #return jsonify(rez)

@app.route('/igre', methods=['GET'])
def get_games():
    f = open("games.csv","r", encoding="utf8")
    mList = []
    for line in f:
        

        try:
            parts = line.split('\t')
            
            game = {
                'Title': parts[1],
                'Developer': parts[3],
                'Publisher': parts[4],
                'Genre(s)': parts[2],
                'Addons': parts[8]
            }
            mList.append(game)
        except Exception as e:
            print(e) 
            pass
    
    
    rez = {'igre':mList[:100]}
    for items in mList:
        print(items)
    return jsonify(rez)


@app.route('/')
def root():
    
    return app.send_static_file('index.html')

app.run(host='0.0.0.0', port=8080, debug=True, use_reloader=False)
